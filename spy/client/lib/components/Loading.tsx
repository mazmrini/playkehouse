import { Grid, LinearProgress } from "@mui/material";
import { SxProps, Theme } from "@mui/material/styles";

interface LoadingProps {
  sx?: SxProps<Theme>;
}

export default ({ sx }: LoadingProps) => (
  <Grid sx={sx} container justifyContent="center">
    <Grid item xs={8}>
      <LinearProgress />
    </Grid>
  </Grid>
);
