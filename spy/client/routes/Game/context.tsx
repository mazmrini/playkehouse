import useAxios from "axios-hooks";
import React, { createContext, useContext, useState } from "react";
import Loading from "../../lib/components/Loading";

interface GameSettings {
  nbPlayers: number;
  nbSpies: number;
}

interface GameContextProps {
  locations: string[];
  setLocations: (locations: string[]) => void;
  settings?: GameSettings;
  setSettings: (gs: GameSettings) => void;
}

const GameContext = createContext({} as GameContextProps);

export const useGame = () => useContext(GameContext);

const useProviderContext = (): GameContextProps => {
  const [locations, setLocations] = useState<string[]>([]);
  const [settings, setSettings] = useState<GameSettings | undefined>();

  return {
    locations,
    setLocations,
    settings,
    setSettings,
  };
};

export const GameProvider = ({ children }: React.PropsWithChildren) => {
  const context = useProviderContext();
  const [{ loading, error }, fetchData, manualCancel] = useAxios(
    "/assets/locations.json",
    {
      manual: true,
      autoCancel: false,
    },
  );

  React.useEffect(() => {
    const fetchCounter = async () => {
      try {
        const response = await fetchData();
        context.setLocations(response.data["locations"]);
      } catch (error) {
        console.error(error);
        manualCancel();
      }
    };

    fetchCounter();
  }, []);

  if (loading || error) {
    return <Loading sx={{ mt: "10vh" }} />;
  }

  return (
    <GameContext.Provider value={context}>{children}</GameContext.Provider>
  );
};
