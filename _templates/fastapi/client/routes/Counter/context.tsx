import useAxios from "axios-hooks";
import React, { createContext, useContext, useState } from "react";
import Loading from "../../lib/components/Loading";

interface CounterContextProps {
  value: number;
  update: (value: number) => void;
}

const CounterContext = createContext({
  value: 0,
} as CounterContextProps);

export const useCounter = () => useContext(CounterContext);

const useProviderContext = (): CounterContextProps => {
  const [counter, setCounter] = useState<number>(0);

  return { update: setCounter, value: counter };
};

export const CounterProvider = ({ children }: React.PropsWithChildren) => {
  const context = useProviderContext();
  const [{ loading, error }, fetchData] = useAxios("/counter", {
    manual: true,
  });

  React.useEffect(() => {
    const fetchCounter = async () => {
      try {
        const response = await fetchData();
        context.update(response.data["counter"]);
      } catch {
        // ignore
      }
    };

    fetchCounter();
  }, []);

  if (loading || error) {
    return <Loading sx={{ mt: "10vh" }} />;
  }

  return (
    <CounterContext.Provider value={context}>
      {children}
    </CounterContext.Provider>
  );
};
