import { AppBar, Toolbar, Typography } from "@mui/material";

export default () => (
  <>
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" color="inherit" component="div">
          Game
        </Typography>
      </Toolbar>
    </AppBar>
  </>
);
