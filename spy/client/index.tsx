import React from "react";
import ReactDOM from "react-dom/client";
import { Helmet } from "react-helmet";
import { RouterProvider } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";
import { router } from "./router";
import { configureAxios } from "./axios";
import { theme } from "./theme";
import { GameProvider } from "./routes/Game/context";
import { assets } from "./lib/assets";
import "./firebase";

configureAxios();

const root_element = document.getElementById("app");
if (root_element === null) {
  throw new Error("Root container missing in index.html");
}

const root = ReactDOM.createRoot(root_element);

root.render(
  <>
    <Helmet>
      <link rel="icon" type="image/webp" href={assets.favicon} />
    </Helmet>
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <GameProvider>
          <RouterProvider router={router} />
        </GameProvider>
      </ThemeProvider>
    </React.StrictMode>
  </>,
);
