import { configure } from "axios-hooks";
import Axios from "axios";

export const configureAxios = () => {
  const axios = Axios.create({
    baseURL: window.origin,
  });

  configure({ axios });
};
