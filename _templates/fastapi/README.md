## Game

Describe me

## Setup

Leverages [bin](https://gitlab.com/mazmrini/bin) that you must first install globally:
```
pip install sbin
```

```
bin up
```

## Run

```
bin run
```

## Development
```
bin s run  # starts server
bin c run  # starts client
```
