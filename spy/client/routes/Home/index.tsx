import { Button, Grid, Typography, TextField } from "@mui/material";
import { useState } from "react";
import { useNavigate } from "react-router";
import { useGame } from "../Game/context";

export default () => {
  const game = useGame();
  const navigate = useNavigate();
  const gameSettings = game.settings || {
    nbPlayers: 4,
    nbSpies: 1,
  };
  const [nbPlayersTxt, setNbPlayers] = useState<string>(
    gameSettings.nbPlayers.toString(),
  );
  const [nbSpiesTxt, setNbSpies] = useState<string>(
    gameSettings.nbSpies.toString(),
  );
  const [error, setError] = useState<boolean>(false);

  const onPlayerChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNbPlayers(event.target.value);
  };

  const onSpyChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNbSpies(event.target.value);
  };

  const onStartGame = () => {
    if (nbPlayersTxt.length === 0 || nbSpiesTxt.length === 0) {
      setError(true);
      return;
    }
    const nbPlayers = Math.floor(Number(nbPlayersTxt));
    const nbSpies = Math.floor(Number(nbSpiesTxt));
    if (nbSpies >= nbPlayers) {
      setError(true);
      return;
    }
    if (nbPlayers <= 0 || nbSpies <= 0) {
      setError(true);
      return;
    }

    game.setSettings({ nbPlayers, nbSpies });
    navigate("/game");
  };

  return (
    <Grid container direction="column" alignItems="center" spacing={5}>
      <Grid item>
        <Typography variant="h4">Configurer la partie</Typography>
      </Grid>
      <Grid item container spacing={3} direction="column" alignItems="center">
        <Grid item>
          <TextField
            error={error}
            variant="outlined"
            label="# Joueurs"
            type="number"
            value={nbPlayersTxt}
            helperText={error ? "Vérifier la valeur" : "Incluant les espions"}
            onChange={onPlayerChange}
          />
        </Grid>
        <Grid item>
          <TextField
            error={error}
            variant="outlined"
            label="# Espions"
            type="number"
            value={nbSpiesTxt}
            onChange={onSpyChange}
            helperText={error && "Vérifier la valeur"}
          />
        </Grid>
      </Grid>
      <Grid item>
        <Button variant="contained" onClick={onStartGame}>
          Commencer
        </Button>
      </Grid>
    </Grid>
  );
};
