import React from "react";
import ReactDOM from "react-dom/client";
import { Helmet } from "react-helmet";
import { RouterProvider } from "react-router-dom";
import { ThemeProvider } from "@mui/material/styles";
import { router } from "./router";
import { configureAxios } from "./axios";
import { theme } from "./theme";
import { assets } from "./lib/assets";

configureAxios();

const root_element = document.getElementById("app");
if (root_element === null) {
  throw new Error("Root container missing in index.html");
}

const root = ReactDOM.createRoot(root_element);

root.render(
  <>
    <Helmet>
      <link rel="icon" type="image/webp" href={assets.favicon} />
    </Helmet>
    <React.StrictMode>
      <ThemeProvider theme={theme}>
        <RouterProvider router={router} />
      </ThemeProvider>
    </React.StrictMode>
  </>,
);
