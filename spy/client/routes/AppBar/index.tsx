import { AppBar, Toolbar, Typography, Grid } from "@mui/material";
import RefreshIcon from "@mui/icons-material/RefreshOutlined";
import { useLocation, useNavigate } from "react-router";

export default () => {
  const navigate = useNavigate();
  const location = useLocation();

  return (
    <AppBar position="static" sx={{ mb: "1rem" }}>
      <Toolbar>
        <Grid container justifyContent="space-between">
          <Typography variant="h6" color="inherit" component="div">
            Espion
          </Typography>
        </Grid>
        {location.pathname !== "/" && (
          <Grid item>
            <RefreshIcon onClick={() => navigate("/")} />
          </Grid>
        )}
      </Toolbar>
    </AppBar>
  );
};
