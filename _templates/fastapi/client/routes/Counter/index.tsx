import { Button, Grid } from "@mui/material";
import { useCounter } from "./context";

export default () => {
  const counter = useCounter();

  return (
    <Grid
      container
      columnSpacing={3}
      justifyContent="center"
      alignItems="center"
    >
      <Grid item>
        <Button
          variant="contained"
          onClick={() => counter.update(counter.value - 1)}
        >
          -
        </Button>
      </Grid>
      <Grid item>{counter.value}</Grid>
      <Grid item>
        <Button
          variant="contained"
          onClick={() => counter.update(counter.value + 1)}
        >
          +
        </Button>
      </Grid>
    </Grid>
  );
};
