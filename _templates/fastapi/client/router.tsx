import Root from "./routes/Root";
import { createBrowserRouter } from "react-router-dom";
import Counter from "./routes/Counter";
import { CounterProvider } from "./routes/Counter/context";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    children: [
      {
        path: "/counter",
        element: (
          <CounterProvider>
            <Counter />
          </CounterProvider>
        ),
      },
    ],
  },
]);
