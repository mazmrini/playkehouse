## Spy

A group of person are in a certain place and a spy is among them.

The spy needs to find the place, the group of folks needs to find the spy.

## Setup

Leverages [bin](https://gitlab.com/mazmrini/bin) that you must first install globally:
```
pip install sbin
```

```
bin up
```

## Run

```
bin run
```

## Development
```
bin c run  # starts client
```
