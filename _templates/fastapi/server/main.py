from typing import Dict, Any
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import random

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins="*",
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/api/ping")
async def ping() -> Dict[str, Any]:
    return {"ping": "pong"}

@app.get("/api/counter")
async def get_counter() -> Dict[str, Any]:
    return {"counter": random.randint(0, 100)}
