import {listCharacters} from './characters.js';

document.addEventListener('DOMContentLoaded', (event) => {
    main();
});

async function main() {
    const baseUrl = window.location.origin;

    const app = document.querySelector("#app");
    app.innerHTML = null;

    (await listCharacters(baseUrl))
        .map((c) => createCard(c.img, c.name))
        .forEach((card) => app.appendChild(card));
}

function createCard(imgUrl, name) {
    const cardEl = document.createElement("div");
    cardEl.className = 'card';
    cardEl.onclick = () => {
        if (!isAlreadyPicked()) {
            cardEl.className = 'card picked';
            makeFirstPick();
            return;
        }

        if (cardEl.className.endsWith(" selected")) {
            cardEl.className = cardEl.className.slice(0, -(" selected".length));
        }
        else {
            cardEl.className += " selected";
        }
    }

    const imgEl = document.createElement("img");
    imgEl.className = 'character';
    imgEl.src = imgUrl;
    const nameEl = document.createElement("div");
    nameEl.textContent = name;

    cardEl.appendChild(imgEl);
    cardEl.appendChild(nameEl)

    return cardEl;
}

function isAlreadyPicked() {
    return document.querySelector("div#picked").className === 'true';
}

function makeFirstPick() {
    document.querySelector("div#picked").className = 'true';
}