import { Visibility, VisibilityOff } from "@mui/icons-material";
import {
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  IconButton,
  Typography,
} from "@mui/material";
import { Navigate } from "react-router-dom";
import { useState, useEffect } from "react";

interface GamePageProps {
  location: string;
  roles: "spy" | "normal"[];
}

export default ({ roles, location }: GamePageProps) => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const [isVisible, setIsVisible] = useState<boolean>(false);

  useEffect(() => {
    setCurrentIndex(0);
    setIsVisible(false);
  }, [location, roles]);

  const onNextClick = () => {
    setIsVisible(false);
    setCurrentIndex((old) => old + 1);
  };

  return (
    <Grid
      container
      columnSpacing={3}
      justifyContent="center"
      alignItems="center"
    >
      {currentIndex >= roles.length && <Navigate to="/" />}
      {currentIndex < roles.length && (
        <Grid item xs={8} lg={4}>
          <Card>
            <CardHeader
              title={`Joueur ${currentIndex + 1}/${roles.length}`}
              action={
                <IconButton onClick={() => setIsVisible((old) => !old)}>
                  {isVisible && <Visibility />}
                  {!isVisible && <VisibilityOff />}
                </IconButton>
              }
            />
            <Divider />
            <CardContent>
              <Grid container direction="column">
                <Grid
                  item
                  container
                  sx={{ height: "5rem", mt: "2rem" }}
                  justifyContent="center"
                >
                  <Grid item>
                    {isVisible && (
                      <Typography sx={{ opacity: 0.65 }} variant="h5">
                        {renderCardText()}
                      </Typography>
                    )}
                  </Grid>
                </Grid>
                <Grid item alignSelf="flex-end" justifySelf="flex-end">
                  <Button
                    sx={{ minWidth: "9rem" }}
                    onClick={onNextClick}
                    variant="contained"
                  >
                    {renderNextButtonText()}
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Grid>
      )}
    </Grid>
  );

  function renderCardText() {
    const currentRole = roles[currentIndex];

    return currentRole === "spy" ? "Vous êtes un espion" : location;
  }

  function renderNextButtonText() {
    const isLastPlayer = currentIndex === roles.length - 1;

    return isLastPlayer ? "Jouer" : "Prochain";
  }
};
