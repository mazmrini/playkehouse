import { Button, Grid, Container } from "@mui/material";
import { Outlet } from "react-router";
import { Link } from "react-router-dom";
import AppBar from "./AppBar";

export default () => (
  <>
    <AppBar />
    <Container maxWidth="lg" sx={{ pt: "1rem", pb: "1rem" }}>
      <Grid container spacing={2}>
        <Grid item>
          <Link to="/">
            <Button sx={{ mb: "2rem" }} variant="contained">
              Home
            </Button>
          </Link>
        </Grid>
        <Grid item>
          <Link to="/counter">
            <Button sx={{ mb: "2rem" }} variant="contained">
              Counter
            </Button>
          </Link>
        </Grid>
      </Grid>
      <div>
        <Outlet />
      </div>
    </Container>
  </>
);
