import Home from "./routes/Home";
import { createBrowserRouter, Outlet } from "react-router-dom";
import Game from "./routes/Game";
import AppBar from "./routes/AppBar";
import { Container } from "@mui/material";

export const router = createBrowserRouter([
  {
    path: "",
    element: (
      <>
        <AppBar />
        <Container maxWidth="lg" sx={{ pt: "1rem", pb: "1rem" }}>
          <Outlet />
        </Container>
      </>
    ),
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/game",
        element: <Game />,
      },
    ],
  },
]);
