import { configure } from "axios-hooks";
import Axios from "axios";

export const configureAxios = () => {
  const axios = Axios.create({
    baseURL: process.env.REACT_APP_API_BASE_URL || `${window.origin}/api`,
  });

  configure({ axios });
};
