# playkehouse

`playkehouse` is a mono-repo full of games to play with friends and family

## Setup

Most, if not all games leverage [bin](https://gitlab.com/mazmrini/bin) that you must first install globally:
```
pip install sbin
```

## Run

### From the game folder
```
cd <game>
bin up
bin run
```

### From the root folder
```
bin run <game_folder>
```

## Create a new game
```
bin new <game_name>
```
