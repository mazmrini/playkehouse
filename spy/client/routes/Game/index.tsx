import arrayShuffle from "array-shuffle";
import { useGame } from "./context";
import { Navigate } from "react-router-dom";
import GamePage from "./GamePage";

export default () => {
  const game = useGame();

  if (!game.settings || game.locations.length === 0) {
    return <Navigate to="/" />;
  }

  const location =
    game.locations[Math.floor(Math.random() * game.locations.length)];
  const nbCitizens = game.settings.nbPlayers - game.settings.nbSpies;
  const roles = arrayShuffle([
    ...new Array(nbCitizens).fill("normal"),
    ...new Array(game.settings.nbSpies).fill("spy"),
  ]);

  return <GamePage location={location} roles={roles} />;
};
